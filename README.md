## PROJECT ##

* ID: **O**pen!**C**OINS
* Contact: schepsen@web.de

## DESCRIPTION ##

This App, called OpenCoins, allows you to track your coin collections, e.g. EU-Coins

## USAGE ##

> mkdir build
>
> cd build
>
> qmake ../src/OpenCoins.pro
>
> make

## SCREENSHOTS ##

![OpenCoins v.0.1.20170907](http://schepsen.eu/software/opencoins/img/opencoins-v.0.1.20170907.png)

## CHANGELOG ##

### Open!COINS 0.2, updated @ 2017-09-10 ###

* Fixed a bug triggered after updating the collection table
* Changed the project structure

### Open!COINS 0.2, updated @ 2017-09-07 ###

* Added a new menubar-Item creates

    * an empty collection

    * an EU-coin collection

### Open!COINS 0.1, updated @ 2017-09-07 ###

* Added new EU-Collections

### Open!COINS 0.1, updated @ 2017-09-04 ###

* Project initialized

## ONGOING CHANGES ##

For more information read the TODO.txt file
