#ifndef COLLECTIONLISTMODEL_H
#define COLLECTIONLISTMODEL_H

#include <QIcon>
#include <QModelIndex>
#include <QSqlError>
#include <QSqlField>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>

class QCollectionListModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    enum CustomRole
    {
        IdRole = Qt::UserRole, NameRole, CountryCodeRole
    };

public:
    explicit QCollectionListModel(QObject *parent = Q_NULLPTR);
    QVariant data(const QModelIndex &item, int role = Qt::DisplayRole) const;
};

#endif // COLLECTIONLISTMODEL_H
