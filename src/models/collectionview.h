#ifndef COLLECTIONVIEW_H
#define COLLECTIONVIEW_H

#include <QSqlQueryModel>

class QCollectionViewModel : public QSqlQueryModel
{
    Q_OBJECT

public:
    explicit QCollectionViewModel(QObject *parent = Q_NULLPTR);    
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex &item, int role = Qt::DisplayRole) const;

};

#endif // COLLECTIONVIEW_H
