#include "collectionview.h"

#include <QVector>
#include <QDebug>
#include <QColor>

QVariant QCollectionViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation == Qt::Horizontal)
    {
        return QSqlQueryModel::headerData(section, orientation, role);
    }
    else
    {
        if(role == Qt::DisplayRole)
        {
            QVector<QVariant> faceValues
            {
                QVariant("0.01"),
                QVariant("0.02"),
                QVariant("0.05"),
                QVariant("0.10"),
                QVariant("0.20"),
                QVariant("0.50"),
                QVariant("1.00"),
                QVariant("2.00")
            };

            return faceValues[section];
        }
        else
        {
            return QSqlQueryModel::headerData(section, orientation, role);
        }
    }
}

QVariant QCollectionViewModel::data(const QModelIndex &item, int role) const
{
    if(role == Qt::TextAlignmentRole)
    {
        return QVariant(Qt::AlignCenter);
    }

    if(role == Qt::BackgroundColorRole && QSqlQueryModel::data(item) == QVariant(-1))
    {
        return QVariant(QColor(238, 238, 238));
    }

    if(role == Qt::DisplayRole && QSqlQueryModel::data(item) == QVariant(-1))
    {
        return QVariant("");
    }

     return QSqlQueryModel::data(item, role);
}

QCollectionViewModel::QCollectionViewModel(QObject *parent) : QSqlQueryModel(parent){ }
