#include "collectionlist.h"

#define shiftRole(role) role - Qt::UserRole

QVariant QCollectionListModel::data(const QModelIndex &item, int role) const
{    
    if(!item.isValid()) { return QVariant(); }

    if(role < Qt::UserRole)
    {
        if(role == Qt::DisplayRole)
        {
            return QSqlQueryModel::data(this->index(item.row(), shiftRole(NameRole)), role);
        }
        if(role == Qt::DecorationRole)
        {
            return QVariant
            (
                QIcon
                (
                    ":/images/countries/" + QSqlQueryModel::data(this->index(item.row(), shiftRole(CountryCodeRole)), Qt::DisplayRole).toString() + ".png"
                )
            );
        }
        return QSqlQueryModel::data(item, role);
    }
    else
    {
        return QSqlQueryModel::data(this->index(item.row(), shiftRole(role)), Qt::DisplayRole);
    }
}

QCollectionListModel::QCollectionListModel(QObject *parent) : QSqlQueryModel(parent) { }
