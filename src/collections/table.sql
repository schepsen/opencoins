BEGIN TRANSACTION;
DROP TABLE IF EXISTS `facevalue`;
CREATE TABLE IF NOT EXISTS `facevalue` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`value`	REAL NOT NULL DEFAULT 0.00
);
INSERT INTO `facevalue` VALUES (1,0.01);
INSERT INTO `facevalue` VALUES (2,0.02);
INSERT INTO `facevalue` VALUES (3,0.05);
INSERT INTO `facevalue` VALUES (4,0.1);
INSERT INTO `facevalue` VALUES (5,0.2);
INSERT INTO `facevalue` VALUES (6,0.5);
INSERT INTO `facevalue` VALUES (7,1.0);
INSERT INTO `facevalue` VALUES (8,2.0);
INSERT INTO `facevalue` VALUES (9,0.0);
DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`code`	INTEGER NOT NULL
);
INSERT INTO `country` VALUES (1,'Belgien','be');
INSERT INTO `country` VALUES (2,'Deutschland','de');
INSERT INTO `country` VALUES (3,'Estland','ee');
INSERT INTO `country` VALUES (4,'Finnland','fi');
INSERT INTO `country` VALUES (5,'Frankreich','fr');
INSERT INTO `country` VALUES (6,'Griechenland','gr');
INSERT INTO `country` VALUES (7,'Irland','ie');
INSERT INTO `country` VALUES (8,'Italien','it');
INSERT INTO `country` VALUES (9,'Lettland','lv');
INSERT INTO `country` VALUES (10,'Litauen','lt');
INSERT INTO `country` VALUES (11,'Luxemburg','lu');
INSERT INTO `country` VALUES (12,'Malta','mt');
INSERT INTO `country` VALUES (13,'Niederlande','nl');
INSERT INTO `country` VALUES (14,'Österreich','at');
INSERT INTO `country` VALUES (15,'Portugal','pt');
INSERT INTO `country` VALUES (16,'Slowakei','sk');
INSERT INTO `country` VALUES (17,'Slowenien','si');
INSERT INTO `country` VALUES (18,'Spanien','es');
INSERT INTO `country` VALUES (19,'Zypern','cy');
INSERT INTO `country` VALUES (20,'Andorra','ad');
INSERT INTO `country` VALUES (21,'Monaco','mc');
INSERT INTO `country` VALUES (22,'San Marino','sm');
INSERT INTO `country` VALUES (23,'Vatikanstadt','va');
DROP TABLE IF EXISTS `collection`;
CREATE TABLE IF NOT EXISTS `collection` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`name`	TEXT NOT NULL,
	`country`	INTEGER NOT NULL,
	`type`	INTEGER NOT NULL DEFAULT 0
);
DROP TABLE IF EXISTS `coin`;
CREATE TABLE IF NOT EXISTS `coin` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`value`	INTEGER NOT NULL,
	`year`	INTEGER NOT NULL,
	`collection`	INTEGER NOT NULL,
	`own`	INTEGER NOT NULL DEFAULT 0,
	`image`	BLOB
);
DROP INDEX IF EXISTS `selectedCoin`;
CREATE UNIQUE INDEX IF NOT EXISTS `selectedCoin` ON `coin` (
	`collection`	ASC,
	`year`	ASC,
	`value`	ASC
);
COMMIT;
