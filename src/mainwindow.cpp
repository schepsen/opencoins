#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;

    db.close();
}

void MainWindow::onActionOpenTriggered()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Ihre Münzen-Sammlung öffnen"), QDir::currentPath(), "Sammlung (*.db *.sqlite *.sqlite3) ;; All Files (*.*)", NULL, NULL);

    db.setDatabaseName(path);

    if (!db.open())
    {
        db.close();
    }
    else
    {
        updateCollectionList(); updateCollectionView();
    }
}

void MainWindow::onActionCreateEmptyTriggered()
{
    createCollection(EmptyType);
}

void MainWindow::onActionCreateEUTriggered()
{
    createCollection(EUType);
}

void MainWindow::onCollectionListDoubleClicked(const QModelIndex &index)
{
    updateCollectionView(index.data(QCollectionListModel::IdRole).toInt());
}

void MainWindow::onCollectionViewClicked(const QModelIndex &index)
{
    QVariant content;

    if((content = index.data(Qt::DisplayRole)) != QVariant(""))
    {
        QSqlQuery query
        (
            QString
            (
                "UPDATE "
                    "coin "
                "SET "
                    "own = %1 "
                "WHERE "
                    "value = %2 AND year = %3 AND collection = %4"
            ).arg
            (
                QString::number((content.toInt() == 1 ? 0 : 1)),
                QString::number(index.row() + 1),
                ui->collectionView->model()->headerData(index.column(), Qt::Horizontal).toString(),
                ui->collectionView->accessibleName()
            )
        );

        query.exec();

        updateCollectionView(ui->collectionView->accessibleName().toInt());
    }
}

void MainWindow::updateCollectionView(int viewId)
{
    if(viewId == 0) // Handle this while opening a new collection set
    {
        ui->fieldCountry->setText("Wählen Sie erst eine Münzen-Sammlung aus");
        ui->fieldOwn->clear();
        ui->fieldValue->clear();
        ui->fieldYears->clear();

        if(ui->collectionView->model() != Q_NULLPTR)
        {
            delete ui->collectionView->model();
        }

        return; // This is a temporary solution! TODO:: reimplement THIS
    }

    QCollectionViewModel *model = new QCollectionViewModel(this);

    QString columns;

    for(int i = 1999; i <= QDate::currentDate().year() + 1; ++i)
    {
        columns.append
        (
            QString("MAX(CASE WHEN coin.year = %1 THEN coin.own ELSE -1 END) [%1], ").arg(i)
        );
    }

    columns.chop(2);

    model->setQuery
    (
        QString
        (
            "SELECT "
                "%1 "
            "FROM "
                "facevalue, coin "
            "WHERE "
                "coin.collection == %2 AND facevalue.id == coin.value "
            "GROUP BY "
                "facevalue.value"
        )
        .arg(columns, QString::number(viewId))
    );

    ui->collectionView->setAccessibleName(QString::number(viewId));
    ui->collectionView->setModel(model);
    ui->collectionView->resizeColumnsToContents();

    QSqlQuery statistics
    (
        QString
        (
            "SELECT "
                "country.name, MIN(coin.year), MAX(coin.year), "
                "SUM(coin.own) AS own, "
                "COUNT(coin.own) AS total, "
                "SUM(CASE WHEN coin.own > 0 THEN facevalue.value END) "
            "FROM "
                "country, collection AS c "
            "LEFT JOIN "
                    "coin ON coin.collection = c.id "
            "LEFT JOIN "
                "facevalue ON coin.value = facevalue.id "
            "WHERE "
                    "c.id = " + QString::number(viewId) + " AND country.id == c.country "
            "GROUP BY country.name"
        )
    );

    if(statistics.exec() && statistics.first())
    {
        ui->fieldCountry->setText(statistics.value(0).toString());
        ui->fieldYears->setText
        (
            QString("%1 - %2").arg
            (
                statistics.value(1).toString(),
                statistics.value(2).toString()
            )
        );
        ui->fieldOwn->setText
        (
            QString("%1 %2 %3").arg
            (
                statistics.value(3).toString(),
                tr("von"),
                statistics.value(4).toString()
            )
        );
        ui->fieldValue->setText(QString("%1 €").arg(QString::number(statistics.value(5).toFloat(), 'g', 5)));
    }
#ifdef QT_DEBUG
    else
    {
        qDebug() << "An error was raised executing: " + statistics.lastQuery();
    }
#endif
}

void MainWindow::updateCollectionList()
{
    QCollectionListModel *model = new QCollectionListModel(this);

    model->setQuery
    (
        "SELECT "
            "c.id, c.name, country.code "
        "FROM "
            "collection AS c, country "
        "WHERE "
            "c.country == country.id "
        "ORDER BY c.name ASC" // use IdRole, DisplayRole, CountryCodeRole
    );

    ui->collectionList->setModel(model);

    ui->statusBar->showMessage("Gespeicherte Münzen-Sammlungen: " + QString::number(model->rowCount()));
}

bool MainWindow::runSQLFile(const QString &filename)
{
    QFile file(filename);
    bool result = true;
    QSqlQuery query;

    file.open(QFile::ReadOnly | QFile::Text);

    auto queries =  file.readAll().split(';');

    for(QString str : queries)
    {
        str = str.simplified();

        if(str.length() > 0)
        {
            result = query.exec(str);
        }
    }

    file.close(); return result;
}

void MainWindow::createCollection(CollectionType type)
{
    QString path = QFileDialog::getSaveFileName(this, tr("Münzen-Sammlung speichern"), QDir::currentPath(), "Sammlung (*.db *.sqlite *.sqlite3) ;; All Files (*.*)", NULL, NULL);

    db.setDatabaseName(path + ".db");

    if(db.open())
    {
        runSQLFile(":/collections/table.sql");

        if(type == EUType)
        {
            runSQLFile(":/collections/eucoins.sql");
        }
    }

    updateCollectionList(); updateCollectionView();
}
