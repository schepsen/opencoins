#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDate>
#include <QDebug>
#include <QFileDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <QSqlTableModel>
#include <QStringList>

#include "models/collectionlist.h"
#include "models/collectionview.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    enum CollectionType
    {
        EmptyType, EUType
    };

    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

private slots:

    void onActionCreateEmptyTriggered();
    void onActionOpenTriggered();
    void onActionCreateEUTriggered();

    void onCollectionListDoubleClicked(const QModelIndex &item);
    void onCollectionViewClicked(const QModelIndex &item);

private:    

    bool runSQLFile(const QString &filename);

    void updateCollectionView(int viewId = 0);
    void updateCollectionList();

    void createCollection(CollectionType type = EUType);

    Ui::MainWindow *ui;

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
};

#endif // MAINWINDOW_H
